# XMLHttpRequest请求在不同域名下会出现"Access Control Allow Origin not allowed by"消息，即使是不同子域名，或根域名和子域名，也会出现这种情况。
# 详情见https://developer.mozilla.org/en/http_access_control
# rails控制器的方法加下以下声明即可

response.header['Access-Control-Allow-Origin'] = '*'
response.header['Content-Type'] = 'text/javascript'